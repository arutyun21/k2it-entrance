﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { AccountService } from '@app/_services';
import { ContactService } from '@app/_services';
import { Contact } from '@app/_models/contact';

@Component({ templateUrl: 'list.component.html' })
export class ListComponent implements OnInit {
    users = null;
    contacts: Contact[] = null;
    JSON: JSON = null;

    constructor(private accountService: AccountService,
        private contactService: ContactService) { }

    ngOnInit() {
        this.accountService.getAll()
            .pipe(first())
            .subscribe(users => this.users = users);

        this.contactService.getContacts()
            .subscribe(contacts => this.contacts = contacts);

        this.JSON = JSON;
    }

    deleteUser(id: string) {
        const user = this.users.find(x => x.id === id);
        user.isDeleting = true;
        this.accountService.delete(id)
            .pipe(first())
            .subscribe(() => {
                this.users = this.users.filter(x => x.id !== id)
            });
    }
}