export interface Contact {
    id: number;
    lastName: string;
    firstName: string;
    patronymic: string;
    organization: string;
    position: string;
    email: string;
    phone: string;
    deleted: boolean;
    userId: number;
}

export interface UpdateContact {
    lastName: string;
    firstName: string;
    patronymic: string;
    organization: string;
    position: string;
    email: string;
    phone: string;
}