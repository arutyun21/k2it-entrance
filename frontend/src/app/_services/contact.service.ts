import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '@app/_models';
import { Contact, UpdateContact } from '@app/_models/contact';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class ContactService {
  private readonly token: string;

  constructor(private http: HttpClient) {
    this.token = (JSON.parse(localStorage.getItem('user')) as User).token;
  }

  getContacts() {
    return this.http.get<Contact[]>(`${environment.apiUrl}/api/contacts/`);
  }

  getById(id: number) {
    return this.http.get<Contact>(`${environment.apiUrl}/api/contacts/${id}/`);
  }

  updateContact(id: number, update: UpdateContact) {
    return this.http.put(`${environment.apiUrl}/api/contacts/${id}/`, update);
  };

  postContact(contact: UpdateContact) {
    return this.http.post<Contact>(`${environment.apiUrl}/api/contacts/`, contact);
  }

  deleteContact(id: number) {
    return this.http.delete<UpdateContact>(`${environment.apiUrl}/api/contacts/${id}/`);
  }
}