﻿import { Component } from '@angular/core';

import { User } from '@app/_models';
import { Contact } from '@app/_models/contact'
import { AccountService, ContactService } from '@app/_services';
import { ClrDatagridSortOrder } from '@clr/angular';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
  selected: Contact[] = [];
  user: User;
  contacts: Contact[] = [
    {
      id: 1, lastName: 'Agafonov', firstName: 'Vladislav', patronymic: 'Yuryevich',
      organization: 'TOO Agaf', position: 'Prodact Manager', email: 'agaf@mail.ru', phone: '8800553535',
      deleted: false, userId: 1
    },
    {
      id: 2, lastName: 'Filimonov', firstName: 'Vladislav', patronymic: 'Yuryevich',
      organization: 'TOO Agaf', position: 'Prodact Manager', email: 'agaf@mail.ru', phone: '8800553535',
      deleted: false, userId: 1
    },
    {
      id: 3, lastName: 'Zenkov', firstName: 'Vladislav', patronymic: 'Yuryevich',
      organization: 'TOO Agaf', position: 'Prodact Manager', email: 'agaf@mail.ru', phone: '8800553535',
      deleted: false, userId: 1
    },
    {
      id: 4, lastName: 'Borisov', firstName: 'Vladislav', patronymic: 'Yuryevich',
      organization: 'TOO Agaf', position: 'Prodact Manager', email: 'agaf@mail.ru', phone: '8800553535',
      deleted: false, userId: 1
    }
  ]

  constructor(
    private accountService: AccountService,
    private contactService: ContactService) {
    this.user = this.accountService.userValue;
    this.contactService.getContacts()
      .subscribe(contacts => this.contacts = contacts);
  }

  onEdit() { }
  onDelete() {
    for (let contactId of this.selected.map(contact => contact.id)) {
      this.contactService.deleteContact(contactId).subscribe(
        _ => this.contacts = this.contacts.filter(contact => contact.id !== contactId)
      );
    }
  }
};