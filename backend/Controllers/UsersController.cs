﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Entities;
using WebApi.Models.Users;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
  [Authorize]
  [ApiController]
  [Route("[controller]")]
  public class UsersController : ControllerBase
  {
    public static readonly string ClaimId = "unique_name";
    public static readonly string ClaimRole = "user_role";
    private IUserService _userService;
    private IMapper _mapper;
    private readonly AppSettings _appSettings;
    private readonly ILogger _logger;
    public UsersController(
        IUserService userService,
        IMapper mapper,
        IOptions<AppSettings> appSettings,
        ILogger<UsersController> logger
        )
    {
      _userService = userService;
      _mapper = mapper;
      _appSettings = appSettings.Value;
      _logger = logger;
    }

    [AllowAnonymous]
    [HttpPost("authenticate")]
    public IActionResult Authenticate([FromBody] AuthenticateModel model)
    {
      var user = _userService.Authenticate(model.Username, model.Password);

      if (user == null)
        return BadRequest(new { message = "Username or password is incorrect" });

      var tokenHandler = new JwtSecurityTokenHandler();
      var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
      var role = user.IsAdmin ? "Admin" : "User";
      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(new Claim[]
          {
                    new Claim(ClaimId, user.Id.ToString()),
                    new Claim(ClaimRole, role)
          }),
        Expires = DateTime.UtcNow.AddDays(7),
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
      };
      var token = tokenHandler.CreateToken(tokenDescriptor);
      var tokenString = tokenHandler.WriteToken(token);

      // return basic user info and authentication token
      return Ok(new
      {
        Id = user.Id,
        Username = user.Username,
        FirstName = user.FirstName,
        LastName = user.LastName,
        IsAdmin = user.IsAdmin,
        Token = tokenString,
      });
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public IActionResult Register([FromBody] RegisterModel model)
    {
      // map model to entity
      var user = _mapper.Map<User>(model);

      try
      {
        // create user
        _userService.Create(user, model.Password);
        return Ok();
      }
      catch (AppException ex)
      {
        // return error message if there was an exception
        return BadRequest(new { message = ex.Message });
      }
    }

    [HttpGet]
    public IActionResult GetAll()
    {
      if (!_userService.isAdmin(Request.Headers))
      {
        return BadRequest(new { message = "You are not admin." });
      }

      var users = _userService.GetAll();
      var model = _mapper.Map<IList<UserModel>>(users);
      return Ok(model);
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
      if (!_userService.isAdmin(Request.Headers))
      {
        return BadRequest(new { message = "You are not admin." });
      }

      var user = _userService.GetById(id);
      var model = _mapper.Map<UserModel>(user);
      return Ok(model);
    }

    [HttpPut]
    public IActionResult Update([FromBody] UpdateModel model)
    {
      if (!_userService.isAdmin(Request.Headers))
      {
        return BadRequest(new { message = "You are not admin." });
      }

      // map model to entity and set id
      var accessToken = Request.Headers[HeaderNames.Authorization].ToString().Split(null)[1];
      _logger.LogDebug($"Token: {accessToken}");
      var id = int.Parse(_userService.GetClaim(accessToken, ClaimId));
      var user = _mapper.Map<User>(model);
      user.Id = id;

      try
      {
        // update user 
        _userService.Update(user, model.Password);
        return Ok();
      }
      catch (AppException ex)
      {
        // return error message if there was an exception
        return BadRequest(new { message = ex.Message });
      }
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      if (!_userService.isAdmin(Request.Headers))
      {
        return BadRequest(new { message = "You are not admin." });
      }

      _userService.Delete(id);
      return Ok();
    }
  }
}
