using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Entities;
using WebApi.Helpers;
using Microsoft.AspNetCore.Authorization;
using WebApi.Models.Contacts;
using Microsoft.Net.Http.Headers;
using WebApi.Services;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
  [Authorize]
  [Route("api/[controller]")]
  [ApiController]
  public class ContactsController : ControllerBase
  {
    private readonly PostgresDataContext _context;
    private IUserService _userService;
    private IMapper _mapper;
    private ILogger _logger;

    public ContactsController(PostgresDataContext context, IUserService userService,
    IMapper mapper, ILogger<ContactsController> logger)
    {
      _context = context;
      _userService = userService;
      _mapper = mapper;
      _logger = logger;
    }

    // GET: api/Contacts
    [HttpGet]
    public async Task<ActionResult<IEnumerable<ContactModel>>> GetContacts()
    {
      var accessToken = Request.Headers[HeaderNames.Authorization].ToString().Split(null)[1];
      var id = int.Parse(_userService.GetClaim(accessToken, UsersController.ClaimId, ""));
      var role = _userService.GetClaim(accessToken, UsersController.ClaimRole, "User");
      var user = _userService.GetById(id);


      if (role == "Admin")
      {
        _logger.LogDebug("Admin Get Request");
        return await _context.Contacts.Select(c => _mapper.Map<ContactModel>(c)).ToListAsync();
      }
      return await _context.Contacts.Where(c => c.UserId == id && !c.Deleted).Select(c => _mapper.Map<ContactModel>(c)).ToListAsync();
      // return user.Contacts.Select(c => _mapper.Map<ContactModel>(c)).ToList();
    }

    // GET: api/Contacts/5
    [HttpGet("{id}")]
    public async Task<ActionResult<ContactModel>> GetContact(int id)
    {
      var contact = await _context.Contacts.FindAsync(id);
      var accessToken = Request.Headers[HeaderNames.Authorization].ToString().Split(null)[1];
      var role = _userService.GetClaim(accessToken, UsersController.ClaimRole, "User");
      var userId = int.Parse(_userService.GetClaim(accessToken, UsersController.ClaimId, ""));

      if (contact == null)
      {
        return NotFound();
      }

      if (role == "User" && userId != contact.UserId)
      {
        return BadRequest(new { message = "This is not your contact." });
      }

      return _mapper.Map<ContactModel>(contact);
    }

    // PUT: api/Contacts/5
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPut("{id}")]
    public async Task<IActionResult> PutContact(int id, UpdateModel update)
    {
      var accessToken = Request.Headers[HeaderNames.Authorization].ToString().Split(null)[1];
      var role = _userService.GetClaim(accessToken, UsersController.ClaimRole, "User");
      var userId = int.Parse(_userService.GetClaim(accessToken, UsersController.ClaimId));
      var contact = await _context.Contacts.FindAsync(id);

      if (contact == null)
      {
        return NotFound();
      }

      if (role == "User" && contact.UserId != userId)
      {
        return BadRequest(new { message = "This is not your contact." });
      }

      foreach (var item in typeof(UpdateModel).GetProperties())
      {
        var o = item.GetValue(update);
        if (o == null)
          continue;
        var p = contact.GetType().GetProperty(item.Name);
        _logger.LogDebug($"name, value: {item.Name}, {o}");
        if (p != null)
        {
          _logger.LogDebug($"Changing value {item.Name}");
          Type t = Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType;
          object safeValue = (o == null) ? null : Convert.ChangeType(o, t);
          p.SetValue(contact, safeValue);
        }
      }

      _context.Entry(contact).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ContactExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Contacts
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPost]
    public async Task<ActionResult<Contact>> PostContact([FromBody] PostModel model)
    {
      var contact = _mapper.Map<Contact>(model);
      var accessToken = Request.Headers[HeaderNames.Authorization].ToString().Split(null)[1];
      var userId = int.Parse(_userService.GetClaim(accessToken, UsersController.ClaimId));
      contact.UserId = userId;
      _context.Contacts.Add(contact);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetContact", new { id = contact.Id },
      _mapper.Map<ContactModel>(contact));
    }

    // DELETE: api/Contacts/5
    [HttpDelete("{id}")]
    public async Task<ActionResult<ContactModel>> DeleteContact(int id)
    {
      var contact = await _context.Contacts.FindAsync(id);

      if (contact == null)
      {
        return NotFound();
      }

      var accessToken = Request.Headers[HeaderNames.Authorization].ToString().Split(null)[1];
      var userId = int.Parse(_userService.GetClaim(accessToken, UsersController.ClaimId));
      var role = _userService.GetClaim(accessToken, UsersController.ClaimRole, "User");

      if (role == "User" && contact.UserId != userId)
      {
        return BadRequest(new { message = "This is not your contact." });
      }

      contact.Deleted = true;
      _context.Entry(contact).State = EntityState.Modified;
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ContactExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return _mapper.Map<ContactModel>(contact);
    }

    private bool ContactExists(int id)
    {
      return _context.Contacts.Any(e => e.Id == id);
    }
  }
}
