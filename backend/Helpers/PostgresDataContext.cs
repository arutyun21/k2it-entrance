using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Services;

namespace WebApi.Helpers
{
  public class PostgresDataContext : DbContext
  {
    protected readonly IConfiguration Configuration;
    private readonly AppSettings _appSettings;

    public PostgresDataContext(
      IConfiguration configuration,
      IOptions<AppSettings> appSettings)
    {
      Configuration = configuration;
      _appSettings = appSettings.Value;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
      // connect to sql server database
      options.UseNpgsql(Configuration.GetConnectionString("WebApiDatabase"));
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      byte[] passwordHash, passwordSalt;
      UserService.CreatePasswordHash(_appSettings.AdminPassword, out passwordHash, out passwordSalt);

      // Don't forget to run migrations, if admin creds were changed.
      modelBuilder.Entity<User>().HasData(new User
      {
        Id = -1,
        FirstName = _appSettings.AdminFirstName,
        LastName = _appSettings.AdminLastName,
        Username = _appSettings.AdminPassword,
        PasswordHash = passwordHash,
        PasswordSalt = passwordSalt,
        IsAdmin = true
      });
    }
    public DbSet<User> Users { get; set; }
    public DbSet<Contact> Contacts { get; set; }
  }
}
