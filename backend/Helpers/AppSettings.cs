namespace WebApi.Helpers
{
  public class AppSettings
  {
    public string Secret { get; set; }
    public string AdminUsername { get; set; }
    public string AdminPassword { get; set; }
    public string AdminFirstName { get; set; }
    public string AdminLastName { get; set; }
  }
}