using Microsoft.EntityFrameworkCore;
using WebApi.Entities;

namespace WebApi.Helpers
{
  public class DefaultContext : DbContext
  {
    public DefaultContext() { }
    public DefaultContext(DbContextOptions<DefaultContext> options)
           : base(options)
    {
    }
    public DbSet<User> Users { get; set; }
    public DbSet<Contact> Contacts { get; set; }
  }
}