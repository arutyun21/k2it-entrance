using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace WebApi.Helpers
{
  public class SqliteDataContext : PostgresDataContext
  {
    public SqliteDataContext(IConfiguration configuration, IOptions<AppSettings> appSettings)
    : base(configuration, appSettings) { }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
      // connect to sqlite database
      options.UseSqlite(Configuration.GetConnectionString("WebApiDatabase"));
    }
  }
}