﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations.SqliteMigrations
{
    public partial class AddedAdminSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "FirstName", "IsAdmin", "LastName", "PasswordHash", "PasswordSalt", "Username" },
                values: new object[] { -1, null, true, null, new byte[] { 100, 51, 115, 95, 31, 11, 221, 149, 255, 6, 1, 45, 71, 46, 172, 46, 162, 176, 109, 71, 44, 245, 68, 157, 194, 51, 102, 22, 4, 3, 233, 172, 137, 194, 239, 169, 79, 25, 101, 22, 67, 24, 126, 75, 22, 121, 20, 227, 158, 18, 217, 191, 113, 17, 161, 78, 40, 40, 111, 161, 35, 56, 110, 243 }, new byte[] { 241, 159, 232, 125, 35, 215, 26, 223, 237, 188, 19, 11, 48, 147, 69, 118, 226, 135, 91, 98, 68, 220, 149, 166, 180, 50, 3, 87, 90, 240, 175, 246, 50, 160, 197, 40, 35, 82, 217, 5, 162, 156, 182, 250, 4, 18, 146, 161, 139, 161, 46, 242, 80, 246, 166, 22, 25, 10, 185, 94, 27, 156, 166, 46, 120, 229, 50, 93, 190, 47, 160, 67, 239, 150, 144, 7, 0, 11, 246, 164, 222, 89, 98, 230, 62, 252, 197, 204, 37, 81, 143, 160, 58, 216, 107, 145, 113, 109, 152, 228, 84, 217, 129, 55, 222, 134, 190, 157, 154, 153, 33, 121, 127, 91, 52, 65, 130, 230, 202, 96, 161, 74, 175, 79, 79, 186, 24, 72 }, "admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: -1);
        }
    }
}
