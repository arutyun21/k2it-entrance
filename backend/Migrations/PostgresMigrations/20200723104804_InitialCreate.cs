﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace WebApi.Migrations.PostgresMigrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<byte[]>(nullable: true),
                    PasswordSalt = table.Column<byte[]>(nullable: true),
                    IsAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Patronymic = table.Column<string>(nullable: true),
                    Organization = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contacts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "FirstName", "IsAdmin", "LastName", "PasswordHash", "PasswordSalt", "Username" },
                values: new object[] { -1, "Hugh", true, "Hefner", new byte[] { 76, 2, 184, 212, 134, 180, 20, 149, 191, 65, 85, 194, 239, 177, 193, 199, 17, 124, 104, 138, 186, 93, 13, 55, 17, 187, 247, 130, 248, 49, 40, 141, 38, 183, 90, 89, 4, 244, 81, 155, 62, 180, 68, 128, 12, 14, 30, 174, 60, 125, 108, 205, 73, 219, 173, 153, 190, 228, 29, 234, 52, 24, 36, 76 }, new byte[] { 39, 4, 209, 2, 59, 139, 58, 53, 247, 183, 51, 76, 178, 145, 36, 219, 185, 148, 94, 210, 89, 140, 159, 103, 112, 78, 142, 178, 20, 84, 185, 18, 114, 5, 104, 81, 44, 84, 190, 44, 126, 30, 125, 179, 10, 232, 160, 223, 86, 217, 4, 225, 178, 78, 145, 26, 60, 94, 240, 25, 20, 8, 160, 156, 169, 87, 130, 129, 143, 98, 244, 251, 167, 252, 140, 224, 204, 146, 250, 22, 73, 35, 138, 199, 185, 153, 146, 54, 164, 40, 220, 237, 241, 115, 220, 116, 172, 132, 234, 4, 225, 69, 12, 252, 11, 186, 155, 96, 236, 52, 24, 160, 43, 227, 93, 0, 232, 0, 119, 231, 62, 162, 107, 191, 109, 224, 28, 13 }, "admin" });

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_UserId",
                table: "Contacts",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
