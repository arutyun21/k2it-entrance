using Microsoft.EntityFrameworkCore;

namespace WebApi.Entities
{
  public class Contact
  {
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Patronymic { get; set; }
    public string Organization { get; set; }
    public string Position { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public bool Deleted { get; set; }

    public int UserId { get; set; }
    public User User { get; set; }

    Contact()
    {
      Deleted = false;
    }
  }
}