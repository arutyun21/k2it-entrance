namespace WebApi.Models.Contacts
{
  public class UpdateModel
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Patronymic { get; set; }
    public string Organization { get; set; }
    public string Position { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
  }
}
