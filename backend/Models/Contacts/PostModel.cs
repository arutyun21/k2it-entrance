using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Contacts
{
  public class PostModel
  {
    [Required]
    public string FirstName { get; set; }
    [Required]
    public string LastName { get; set; }
    [Required]
    public string Patronymic { get; set; }
    [Required]
    public string Organization { get; set; }
    [Required]
    public string Position { get; set; }
    [Required]
    public string Email { get; set; }
    [Required]
    public string Phone { get; set; }
  }
}
